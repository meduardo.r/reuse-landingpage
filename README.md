# reuse-landingpage

## Pagina do Trabalho do Treinamento EJCM (landping page - Reus-e)

Esse repositório é dedicado a um treinamento com viés educacional para uma empresa Júnior (EJCM) da UFRJ, à ideia é o aprimoramento dos conhecimentos, dos candidatos através de palestras ilustrativas, aulas guiadas e praticas de forma interativa via internet com a plataforma (Discord).


> Uso do Git, através dessa plataforma GII-LAB com ensinamento de boas praticas, para melhor utilização da ferramenta e ter melhor resultado nos projetos.

> Uso Figma para produção dos design dos produtos (mobile, sites, sistemas web)

> Front-End e Backend 


### Projeto Landing Page.

A landing page é uma página da web autônoma criada para atingir um objetivo: a conversão. **É uma ótima estratégia de marketing e pode ser usada em campanhas publicitárias.** Essa página pode aparecer em resposta a um clique em um resultado otimizado para motores de busca, campanhas de e-mail marketing, de redes sociais ou anúncios on-line. Esse tipo de marketing é fundamental para gerar leads, alcançar os clientes certos e nortear a sua estratégia de marketing geral.

> O projeto é criar uma landing page de marketplace (reus-e) com apresentação concisa e direta que traga uma ideia de como será o produto final e algumas de suas funcionalidades



### Tarefas
    
    * [x] Criar outras paginas (Produtos, Entrar "login", Sobre), finalizar as funcionalidades da Home.
    * [x] Estudar as documentação sobre as funcionalidades desejadas.
    * [x] Criar um carrosel com as imagens dos produtos.
    * [x] Exportar as imagens do wireframe de alta fidelidade para pagina Home.
    * [x] Formulário  Modal de tela de login.   
