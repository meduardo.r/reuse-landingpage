
// Variaveis (tempo de transição e index da nodelist)
let time = 7000;
let indexImage = 0;

// Inicialização da função
carrossel();


// Função da transição das fotos
function carrossel() {
    let i;
    let images = document.getElementsByClassName("selected");
    
    // verifica todos os itens da nodelist
    for (i = 0; i < images.length; i++) {
        images[i].style.display = "none";
    }

    // Condição para transição das imagens da nodelist
    indexImage++
    if (indexImage > images.length) {indexImage = 1;}
    images[indexImage-1].style.display = "block";

    // Seta o tempo da transição das fotos 
    setTimeout(carrossel, time);
}


/**Modal Page: Tela de Cadastro*/



// Constantes (botões e a tela modal)
const modal = document.querySelector(".modal");
const openModalBtn = document.querySelector(".btn-open");
const closeModalBtn = document.querySelector(".btns-canc");


// Função de abertura da tela modal
const openModal = function() {
    modal.classList.remove("hidden");

}
openModalBtn.addEventListener("click", openModal);



// Função para Fechar Tela Modal
const closeModal = function() {
    modal.classList.add("hidden");
}

closeModalBtn.addEventListener("click", closeModal);